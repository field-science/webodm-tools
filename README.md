# WebODM Tools
Our collection of WebODM tools, scripts, configs, and info.

## Presets
WebODM uses an array of arguements to pass flags to processing nodes. When configuring a preset, use the format:

`[{"name": "YOUR_FLAG_NAME", "value": "YOUR_FLAG_VALUE"}...]`

When run, this will translate to the CLI equivalent of:

`... --YOUR_FLAG_NAME YOUR_FLAG VALUE ...`
